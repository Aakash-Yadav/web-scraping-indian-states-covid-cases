#Import Requests ,BeautifulSoup
from requests import get 
from bs4 import BeautifulSoup 
import csv 
def covid_india():
    html_data = get('https://prsindia.org/covid-19/cases').text
    return html_data

def beautifulSoup_data():
    soup = BeautifulSoup(covid_india(),'lxml')
    return soup

def confirmed_cases():
    'open csv file for saving data '
    csv_file= open('covid_india.csv','w')
    csv_writer = csv.writer(csv_file)
    'append Row'
    csv_writer.writerow(['Srno','State/UT','Confirmed Cases','Active Cases','Cured/Discharged','Death'])
    'called fun (bs4 data )'
    data = beautifulSoup_data()
    v= data.find('table',class_='table table-striped table-bordered')
    'loop to table tage'
    for tem in v.find_all('tbody'):
        'Tr tage'
        row = tem.find_all('tr')
        'loop every row and collumn'
        for sr_num,State,Confirmed,Active,Discharged,Death in row:
            'append data to csv'
            csv_writer.writerow([sr_num.text,State.text,Confirmed.text,Active.text,Discharged.text,Death.text])
    'close the csv file'
    csv_file.close()
    print()
    print('Done')

if __name__ == '__main__':
    confirmed_cases()

